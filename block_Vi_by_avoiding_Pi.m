function [v1,v2]=block_Vi_by_avoiding_Pi(sysmat,rhs1,rhs2)
% The purpose of this function is to construct the blocks V_i=[v1,v2] of the basis matrix V_m
% by avoading the dense projection matrix "Pi" and its Theta-decomposition
% as described in our paper. 
nr=size(rhs1,2);
zeroMat=sparse(sysmat.fulln-sysmat.nv,nr);
v2=sysmat.fullA\[rhs1;zeroMat];
v2=v2(1:sysmat.nv,:);
sysmat.fullA(1:sysmat.nv,1:sysmat.nv)=sysmat.M;
v1=sysmat.fullA\[rhs2;zeroMat];
v1=v1(1:sysmat.nv,:);
end
