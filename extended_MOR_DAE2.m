function [Mm,Am,Bm,Cm]=extended_MOR_DAE2(sysmat,m)
% The purpose of this function is to construct an orthonormal basis matrix Vm
% associated to the extended-Krylov subspace via the Arnoldi algorithm.
% This basis matrix plays the main role in the construction of a reduced
% model to the original one known as the DAE system of index 2.

% Inputs:
% sysmat : a struct contains all the matrices associated to the level of
% discretization and Reynold number chosen.
% m : number of iterations.

% Output:
% Mm, Am, Bm and Cm :  The system matrices of the reduced model.

nb2=2*sysmat.nb;
[v1,v2]=block_Vi_by_avoiding_Pi(sysmat,sysmat.B,sysmat.B);
[Vm(:,1:nb2),~]=QR([v1,v2]);%QR decomposition
for j=1:m
    j1s = (j+1)*nb2; js = j*nb2; 
    js1 = js+1; jms = (j-1)*nb2+1; jsh=(j-1)*nb2+sysmat.nb;
    [Vtilde1,Vtilde2]=block_Vi_by_avoiding_Pi(sysmat,sysmat.M*Vm(:,jsh+1:js),sysmat.A*Vm(:,jms:jsh));
    Vtilde=[Vtilde1,Vtilde2];
    % Gram-Schmidt process
    for i1=1:2
        for kk=1:j
           k1=(kk-1)*nb2+1; k2=kk*nb2;
           coef=Vm(:,k1:k2)'*Vtilde;
           Vtilde=Vtilde-Vm(:,k1:k2)*coef;
        end
    end
    %%%%%%%%%%%%%%%%%%%%%
    if (j<=m)
        [Vm(:,js1:j1s),~]=QR(Vtilde);%QR decomposition
    end
end
Vm=Vm(:,1:2*m*sysmat.nb);
Am=Vm'*(sysmat.A*Vm); Bm=Vm'*sysmat.B; Cm=sysmat.C*Vm; Mm=Vm'*(sysmat.M*Vm);
end
