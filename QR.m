function [Q,R] = QR(A)
% ---------least square program by the Gram schmidt procedure.----------- 
% We try to write A=QR such that A is a matrix with m rows and n columns. 
% Q having m rows and n columns, R square matrix of size n

[m,n] = size(A);
if m<n, fprintf('erreur m >= n.\n'); end
Q = zeros(m,n);
R = zeros(n,n);

for k=1:n
Q(:,k) = A(:,k);
for j=1:k-1
R(j,k) = Q(:,j)'*A(:,k);
Q(:,k) = Q(:,k) - R(j,k)*Q(:,j);
end
R(k,k) = norm(Q(:,k));
Q(:,k) = Q(:,k)/R(k,k);
end