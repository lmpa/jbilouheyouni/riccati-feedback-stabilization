function X = schur_care( A, G, Q )
% 
% X=SCHUR_CARE(A,G,Q) solves the CARE A'X + XA + Q - XGX = 0
% by means of the basic Schur algorithm
%
% A, G, Q: matrix coefficients
% X : solution of the CARE


%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
%IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
%FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
%COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
%IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
%CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
%PERMISSION TO USE, COPY, MODIFY, AND/OR DISTRIBUTE OF THIS SOFTWARE FOR ANY
%PURPOSE WITH OR WITHOUT FEE IS HERYBY GRANTED.
%
% Copyright (C) 2018-20 by P. Kuerschner
	
	n = size( G, 1 );
	H = [A G; Q -A'];
	
	[U, T] = schur( H, 'real' ); % Computing the Schur form of H
	e = ordeig( T );
	[es, is] = sort( real(e), 'ascend' );
	
	sel = zeros( 2*n, 1 );
	sel( is(1:n) ) = 1;
	
	Q = ordschur( U, T, sel ); % Sorting the Schur form of H
	
	X = -Q(n+1:2*n, 1:n) / Q(1:n, 1:n);
end
