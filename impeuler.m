
function [y]=impeuler(E,A,B,C,tau,tmin,tmax,x0,alpha)
% Simple implicit Euler implementation for validation of the DAE2
% MESS open loop example via a basic step response computation
%
%  [y,yr] = impeuler(E,A,B,C,tau,tmin,tmax,x0,alpha)
%
% INPUTS:
% E,A,B,C      The original system matrices
% tau          time step size
% tmin         start time 
% tmax         end time
% x0          initial states of the full order model
% alpha        step height for the input step function
%
% OUTPUT:
% y         outputs of the full and reduced systems in [tmin,tmax]
%

% Author: Jens Saak
  [L,U,P,Q] = lu(E-tau*A);
 
 ntau=ceil((tmax-tmin)/tau);
 y=zeros(size(C,1),ntau);
 for i=1:ntau
    if ~mod(i,ceil(ntau/10))
    end
    if i<0.1*ntau
        x=Q*(U\(L\(P*(E*x0))));
    else
        salpha=smoother(alpha,i,ntau);
        x=Q*(U\(L\(P*(E*x0+(salpha*tau)*sum(B,2)))));
    end
    y(:,i)=C*x;
    x0=x;
 end
  fprintf('\n\n');
end

function alpha= smoother(alpha,i,ntau)
 if i<0.2*ntau
    alpha=sin((10*pi*(i-0.1*ntau)/ntau)-0.5*pi)*alpha;
 end
end
