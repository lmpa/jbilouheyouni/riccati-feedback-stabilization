function [Zm,err] = extended_ricca_DAE2(sysmat,m,tol,dtol,nrm_pi_Ct)
%%The goal from this function is to solve a generalized algebraic Riccati equation by mean
%%of an enxtended Krylov subspace method. We compute a low-rank
%%approximation solution "X=Zm Zm^T" to 
%%Pi A^T X M + M X A Pi^T - M X BB^T X M + Pi C^TC Pi=0.
%%The dense projection matrix Pi is avoided in all our computation as
%%described below and also in our paper.

%%Authors : M.A. Hamadi, K. Jbilou and A. Ratnani
%%Paper: 

%%Inputs:
%%sysmat : a structure contains all the matrices associated to the level of
%%discretization and Reynold number chosen.
%%m : max number of iterations.
%%tol : tolerance set to stop the algorithm, (i.e., ||R(Xm)||/nrmC < tol
%       where R(Xm)=Pi A^T Xm M + M Xm A Pi^T - M X BB^T Xm M + Pi C^TC Pi)
%%dtol : tolerance truncation set to construct Zm.
%%nrm_pi_ct : norm of 'Pi C^TC Pi' computed without involving 'Pi'.

%%Outputs :
%%Zm : the first partition of the approximate solution Xm to X, given by 
%%X=Xm=Zm Zm^T.
%%err: a vector contains the relative error ||R(Xm)||/nrm_pi_ct at each iteration,
%where R(Xm)= Pi A^T Xm M + M Xm A Pi^T - M X BB^T Xm M + Pi C^TC Pi is the
%%residual associated to Xm.

nc=size(sysmat.Ct,2);
nc2=2*nc;
odds=[]; H=sparse(nc2*(m+1),nc2*m); T=sparse(nc2*(m+1),nc2*m);
t=sparse(nc2*(m+1),nc2*m);
%%first block V1=[v1,v2] of the basis matrix Vm
[Vc1,Vc2]=block_Vi_by_avoiding_Pi(sysmat,sysmat.Ct,sysmat.Ct);
[Vm(:,1:nc2),lam]=qr([Vc1,Vc2],0); 

ibeta=inv(lam); lam1=lam(1:nc,1:nc);lam2=lam1*lam1';
err=[];
Bj=Vm(:,1:nc2)'*sysmat.B;
for j=1:m
    j1s = (j+1)*nc2; js = j*nc2; 
    js1 = js+1; jms = (j-1)*nc2+1; jsh=(j-1)*nc2+nc;
    [Vtilde1,Vtilde2]=block_Vi_by_avoiding_Pi(sysmat,sysmat.M*Vm(:,jsh+1:js),sysmat.At*Vm(:,jms:jsh));
    Vtilde=[Vtilde1,Vtilde2];
    %%---Gram-shmidt process---%%
    for i1=1:2
        for kk=1:j 
           k1=(kk-1)*nc2+1; k2=kk*nc2;
           coef=Vm(1:sysmat.nv,k1:k2)'*Vtilde;
           H(k1:k2,jms:js)=H(k1:k2,jms:js)+coef;
           Vtilde=Vtilde-Vm(:,k1:k2)*coef;
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (j<=m)
        [Vm(:,js1:j1s),h]=qr(Vtilde,0);
        H(js1:j1s,jms:js)=h; hinv = inv(h);
    end
    Bj=[Bj;Vm(:,js1:j1s)'*sysmat.B];
    Id = speye(js+nc2);
    if ( j == 1 ) 
       t(1:js+nc,(j-1)*nc+1:j*nc)=[H(1:nc2+nc,1:nc)/ibeta(1:nc,1:nc), ...
           speye(nc2+nc,nc)/ibeta(1:nc,1:nc)]*ibeta(1:nc2,nc+1:nc2); 
    else
       t(1:js+nc2,(j-1)*nc+1:j*nc) = ...
         t(1:js+nc2,(j-1)*nc+1:j*nc) + H(1:js+nc2,jms:jms-1+nc)*rho; 
    end
    odds = [odds, jms:(jms-1+nc)];        
    evens=1:js; evens(odds) = [];       
    T(1:js+nc2,odds) = H(1:js+nc2,odds);      
    T(1:js+nc,evens) = t(1:js+nc,1:j*nc); 
    t(1:js+nc2,j*nc+1:(j+1)*nc) = (Id(1:js+nc2,(js-nc+1):js)- ...
    T(1:js+nc2,1:js)*H(1:js,js-nc+1:js))*hinv(nc+1:nc2,nc+1:nc2); 
    rho = hinv(1:nc,1:nc)\hinv(1:nc,nc+1:nc2);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    CmtCm=full(speye(js,nc)*lam2*speye(js,nc)');
    Bjj=Bj(1:js,:)*Bj(1:js,:)';
    %%------Solving the lower dimensional Riccati equation------%%
    Z=schur_care(full(T(1:js,1:js)'),Bjj,CmtCm);
    nr=norm(T(js+1:js+nc,jms:js)*Z(jms:js,:)); 
    resid_err=nr/nrm_pi_Ct;
    err=[err;resid_err];
    
    if(resid_err <= tol)
       [uZ,sZ]=eig(Z); [sZ,id]=sort(diag(sZ));
       sZ=flipud(sZ); uZ=uZ(:,id(end:-1:1));
       is=sum(abs(sZ) > dtol);
       Z=uZ(:,1:is)*diag(sqrt(sZ(1:is)));
       Zm=Vm(:,1:js)*Z; 
       break,
    else 
        Zm=[];
    end
    
end

end
