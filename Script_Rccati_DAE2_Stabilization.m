%%In this script we solve a generalized algebraic Riccati equation 
       %%Pi A^T X M + M X A Pi^T - M X BB^T X M + Pi C^TC Pi=0.
%%Where Pi is a dense projector matrix that we will avoid during the
%%process of solving the GAR equation.

%%Associated with the following  DAE system of index 2

%[[ M 0] [[d(v(t))/dt]   [[ A G] [[v(t)]   [[B]  
%                =                       +     u(t)   
% [ 0 0]] [d(p(t))/dt]]   [G' 0]] [p](t)]  [0]]   
% y(t)=[C 0][[v]                                   
%            [p]]             


%%We use an extended Krylov subspace method to construct a low-rank
%%approximation "X=Xm=Zm Zm^T" without an explicit computation of the dense
%%projector matrix Pi. This can be done by solving a serie of saddle point problems
%%as described in our paper.

%%Authors : M.A. Hamadi, K. Jbilou and A. Ratnani.
%%Title : An efficient extended block Arnoldi algorithm for feedback 
         %%stabilization of incompressible Navier-Stokes flow problems.
         %%To be published in Applied Numerical Mathematics journal (APNUM).

clear all,
clc,
re=300;%Reynolds number, re={300,400,500}
lvl=1; %Level of discretization
load(sprintf('mat_nse_CLS_re_%d',re))
%%----------- Matrices -------------%%
%%Saving all the matrices and their dimensions in one 'structure' named "sysmat"
sysmat.fullM=mat.mat_v.E{lvl};
sysmat.fullA=mat.mat_v.fullA{lvl}';
sysmat.nv=mat.mat_mg.nv(lvl);
sysmat.M=sysmat.fullM(1:sysmat.nv,1:sysmat.nv); 
sysmat.fulln=size(sysmat.fullM,1);
sysmat.At=sysmat.fullA(1:sysmat.nv,1:sysmat.nv); 
sysmat.B=mat.mat_v.B{lvl}; sysmat.nb=size(sysmat.B,2); 
sysmat.Ct=mat.mat_v.C{lvl}'; sysmat.nc=size(sysmat.Ct,2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%-------Computing the norm of Pi C^tC Pi without involving the dense matrix Pi-----%%
np=sysmat.fulln-sysmat.nv;
A1=sysmat.fullA; A1(1:sysmat.nv,1:sysmat.nv)=sysmat.fullM(1:sysmat.nv,1:sysmat.nv); 
X=A1\[sysmat.Ct;sparse(np,sysmat.nc)]; 
X=X(1:sysmat.nv,:); nrmC=norm(full(X))^2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m=120;% max number of iterations
tol=1e-7; % tolerance set to stop the algorithm
dtol=1e-12; % tolerance truncation set to construct Zm (Xm=Zm Zm^T) 

tic
[Zm,err]=extended_ricca_DAE2(sysmat,m,tol,dtol,nrmC);
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%After computing the approximation solution X=Xm=Zm Zm^T, we can now 
%%construct the feedback matrix K=-B^T Xm M=-B^T Zm Zm^T M, that will be
%%included afterwards in the original unstable system to stabilize it, especially when
%%we choose Re=400 or 500.

%%We did a time domaine response simulation using an implicit Euler
%%implementation for validation and also to show the results before and
%%after stabilization
%%----Inputs----%%
alpha=1;%step height for the input step function
tau=1e-2;%time step size
tmin=0;%start time 
tmax=40;%end time
x0=zeros(sysmat.fulln,1);%initial state of the full
T=tmin:tau:tmax;
ntau=floor((tmax-tmin)/tau);
range=1:floor(ntau/500):ntau;
fprintf('\n\n')
fprintf('-------Plot the numerical results (it may take a few minutes)--------')
%%---Time domaine simulation for the unstable system
nvp=sysmat.fulln-sysmat.nv;
sysmat.B1=[sysmat.B; sparse(nvp,sysmat.nb)];
sysmat.C1=[sysmat.Ct', sparse(sysmat.nc,nvp)];
y=impeuler(sysmat.fullM,sysmat.fullA',sysmat.B1,sysmat.C1,tau,tmin,tmax,x0,alpha);
Km=sysmat.B'*(Zm*(Zm'*sysmat.M));
sysmat.fullA(1:sysmat.nv,1:sysmat.nv)=sysmat.fullA(1:sysmat.nv,1:sysmat.nv)'-sysmat.B*Km;
%%---Time domaine simulation for the stabilized system
y1=impeuler(sysmat.fullM,sysmat.fullA,sysmat.B1,sysmat.C1,tau,tmin,tmax,x0,alpha);
subplot(2,1,1)
plot(T(range),y(1,range))
xlabel('time')
ylabel('magnitude of output')
title('before stabilization')
subplot(2,1,2)
plot(T(range),y1(1,range))
xlabel('time')
ylabel('magnitude of output')
title('after stabilization')