% Computes a reduced model to the original system of differential algebraic equations
% with index 2 (DAE-inedx 2) that we get after a discretization of Navier-Stokes 
% equations.

%[[ M 0] [[d(v(t))/dt]   [[ A G] [[v(t)]    [[B]  
%                =                       +       u(t)   
% [ 0 0]] [d(p(t))/dt]]   [G' 0]] [p](t)]   [0]]    %%The original DAE system%%
% y(t)=[C 0][[v]                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%
%            [p]]             

% Our method is based on a projection technique onto an extended block Krylov subspace
% that appropriately allows us to construct the following reduced system 

% Mm (t) = Am v_1(t) +Bm u(t)   %%%%The reduced system%%%%
% y(t)=Cm v_1(t)                %%%%%%%%%%%%%%%%%%%%%%%%%%


% For more details , please refer to our paper
% Authors : M.A. Hamadi, K. Jbilou and A. Ratnani.
% Title : An efficient extended block Arnoldi algorithm for feedback 
         %%stabilization of incompressible Navier-Stokes flow problems.
         %%To be published in Applied Numerical Mathematics journal (APNUM).

clear all,
clc,
re=300;%Reynolds number, re={300,400,500}
lvl=1; %Level of discretization
load(sprintf('mat_nse_CLS_re_%d',re))
%%%%%%%% Matrices %%%%%%%%
sysmat.fullM=mat.mat_v.E{lvl};
sysmat.fullA=mat.mat_v.fullA{lvl};
sysmat.nv=mat.mat_mg.nv(lvl);
sysmat.M=sysmat.fullM(1:sysmat.nv,1:sysmat.nv); 
sysmat.fulln=size(sysmat.fullM,1);
sysmat.A=sysmat.fullA(1:sysmat.nv,1:sysmat.nv); 
sysmat.B=mat.mat_v.B{lvl}; sysmat.nb=size(sysmat.B,2); 
sysmat.C=mat.mat_v.C{lvl}; sysmat.nc=size(sysmat.C,1);
%%%%%%%%%%% Constructing the reduced model using Algorithm 2%%%%%%%%%%%%%%
m=100; % number of iterations (2*m*nb is the dimension of the Krylov subspace)
tic
[Mm,Am,Bm,Cm]=extended_MOR_DAE2(sysmat,m);
tf=toc;
fprintf(['\n The computation time needed to construct Vm basis matrix of our Krylov subspace '...
                                                             'is\n %d \n'],tf) 

%%%%%%%%%%%%%%%%%%%%%%%%%% Transfer functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n');
freq=lp_lgfrq(1e-5,1e5);%sampled frequencies
nvp=sysmat.fulln-sysmat.nv;
sysmat.B=[sysmat.B; sparse(nvp,sysmat.nb)];
sysmat.C=[sysmat.C, sparse(sysmat.nc,nvp)];
fprintf('\nComputing and plotting the two transfer functions, original and reduced \n');
%%Computing the original and reduced transfer functions at each frequency freq(i)
tfF=lp_trfia(freq,sysmat.fullA,sysmat.B,sysmat.C,[],sysmat.fullM);
tfFm=lp_trfia(freq,Am,Bm,Cm,[],Mm);
nrm_tfF=lp_gnorm(tfF,sysmat.nb,sysmat.nc);
nrm_tfFm=lp_gnorm(tfFm,sysmat.nb,sysmat.nc);
%%The error norm betweenn the two transfer functions
norm_error=lp_gnorm(tfF-tfFm,sysmat.nb,sysmat.nc);

%%%%%%%%%%%%%%%%%%%%--Ploting---%%%%%%%%%%%%%%%%%%%%%%%%
subplot(1,2,1)
loglog(freq,nrm_tfF,'o')
hold on
loglog(freq,nrm_tfFm)
xlabel('frequencies')
ylabel('\sigma_{max}(F(i\omega)) and \sigma_{max}(F_m(i\omega))')
legend('Original model', 'Reduced model')
title('bode plot')
subplot(1,2,2)
loglog(freq,norm_error')
xlabel('frequencies')
ylabel('\sigma_{max}(F(i\omega)- F_m(i\omega))')
title('Absolute error')
