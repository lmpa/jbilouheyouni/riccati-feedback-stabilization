Riccati_feedback_stabilization_using_Extended_Arnoldi_method is a MATLAB program in which we solve two problems : the first concerns the construction of an efficient reduced system of the original DAE system using a projection technique onto an extended block Krylov subspace. The second problem concerns the solution of a Linear Quadratic Regulator (LQR) problem based on a Riccati feedback approach using numerical solutions of large-scale algebraic Riccati equations. 

THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED WARRANTY.                                                          
ANY USE OF THE SOFTWARE CONSTITUTES  ACCEPTANCE OF THE TERMS OF THE ABOVE STATEMENT.

# Authors 
                                           
     M. HEYOUNI                            
     Université du Littoral, Calais, France
     E-mail: mohammed.heyouni@univ-littoral.fr
     
     K. JBILOU                                                            
     Université du Littoral, Calais, France                               
     E-mail: khalide.jbilou@univ-littoral.fr   

     A. RATNANI
     Université polytechnique Mohammed VI, Maroc

# Datas 

Due to the large size of the data matrices, we were unable to add them
with MATLAB programs in a one folder. To download the matrices please follow the following links
 -https://zenodo.org/record/3555368#.YY_Mr57MJPY  (Data presented in Table 1)
 -https://www.mpi-magdeburg.mpg.de/projects/mess. (Data presented in Table 2)
